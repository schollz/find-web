+++
title = "Legal"
date = "2016-12-25"
sidemenu = "true"
description = "Privacy Policy and Licensing for FIND"
keywordlist = "privacy, policy, legal, internal nagivation, indoor positioning, positioning"
+++

- [Privacy Policy](#privacy-policy)
- [License](#license)

# Privacy Policy

This Privacy Policy governs the manner in which internalpositioning.com collects, uses, maintains and discloses information collected from users (each, a "User") of the https://ml.internalpositioning.com website ("Site"). This privacy policy applies to the Site and all products and services offered by ml.internalpositioning.com.

### Personal identification information

We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may visit our Site anonymously. We will collect personal identification information from Users ONLY if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.

### Non-personal identification information

We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.

### Web browser cookies

Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.

### How we use collected information

The Site may collect and use Users personal information for the following purposes:

- To personalize user experience. We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.
- To improve our Site. We may use feedback you provide to improve our products and services.

### How we protect your information

We adopt appropriate data collection, storage and processing practices and security measures to protect personal information and transaction information stored on our Site. However, **the Site does not make use of User authentication. Any User data submitted to the Site is not protected from access, disclosure, alteration or destruction by another User. We recommend using the Site for demonstration purpose only, and we recommend not sharing highly sensitive or private information with the site and to use a Group ID that is uncommon and difficult to guess.**

### Sharing your personal information

We DO NOT sell, trade, or rent Users personal identification information, or the generic aggregated demographic information, to others.

### Changes to this privacy policy

The Site has the discretion to update this privacy policy at any time. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.

### Your acceptance of these terms

By using this Site, you signify your acceptance of this policy as well as the terms of the [license](https://github.com/schollz/find/blob/master/LICENSE) for use and distribution of the software running the Site (including the Limitation of Liability). If you do not agree to this, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.

# License

## FIND

**FIND** is a Framework for Internal Navigation and Discovery.

Copyright (C) 2015-2016 Zack Scholl

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the [GNU Affero General Public License](LICENSE) for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see [GNU Affero General Public License here](https://www.gnu.org/licenses/agpl.html).

## CanvasJS

**FIND** uses [CanvasJS](http://canvasjs.com/). Note that you will have to buy the appropriate CanvasJS License if you use this software for commercial purposes. CanvasJS has the following Dual Licensing Model:

### Commercial License

Commercial use of CanvasJS requires you to purchase a license. Without a commercial license you can use it for evaluation purposes only. Please refer to the following link for further details: http://canvasjs.com/.

### Free for Non-Commercial Use

For non-commercial purposes you can use the software for free under Creative Commons Attribution-NonCommercial 3.0 License.

A credit Link is added to the bottom right of the chart which should be preserved. Refer to the following link for further details on the same: http://creativecommons.org/licenses/by-nc/3.0/deed.en_US.
