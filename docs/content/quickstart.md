+++
title = "Quickstart"
date = "2016-12-25"
sidemenu = "true"
description = "Quickstart to track your devices"
thumbnail = "https://www.internalpositioning.com/wifiicon.png"
keywordlist = "steps, getting started, golang, install, internal nagivation, indoor positioning, positioning, android, app"
+++

<div class="pure-g">
        <div class="pure-u-19-24">
<p>The easiest way to get started is to use the Android App or download a program onto your laptop computer. FIND works with many other devices as well, including <a href="/client/#electric-imp">Electric Imps</a>, <a href="/client/#raspberry-pi">Raspberry Pis</a>, <a href="/client/#particle-photon">Particle Photons</a>, and <a href="/client/#esp8266">ESP8266 chips</a>. There is <a href="/client/">more information about other devices here</a></p>
<ul>
<li><a href="#1--download-the-software">1. Download the software</a></li>
<li><a href="#2--gather-fingerprint-data">2. Gather fingerprint data</a></li>
<li><a href="#3--track-yourself">3. Track yourself</a></li>
</ul>
        </div>
        <div class="pure-u-1-24"></div>
        <div class="pure-u-4-24"><img class="pure-img-responsive" src="/wifiicon.png"></div>
</div>


## 1\. Download the software

**Android users:** [download the current version of the app](https://play.google.com/store/apps/details?id=com.hcp.find). _Sorry iPhone users but [the Apple store prevents apps that access WiFi information](https://doc.internalpositioning.com/faq/#can-i-use-an-iphone), so I will be unable to release a iPhone version._

**OR**

**Computer users:** you can [download the current version of the fingerprinting program](https://github.com/schollz/find/releases/tag/v0.5), available for Rasbperry Pi, OSX, Linux, and Windows.

## 2\. Gather fingerprint data

First, to get started using **FIND** you will need to gather fingerprint data in your locations.

**Android users:** When you start up the app you will be asked for a username (enter whatever you want) and you'll be assigned a unique group name. Simply click "Learn" and you'll be prompted for a location name. After you enter a location, the app will connect to the server and then submit fingerprints.

<center><img src="https://i.imgur.com/fbcYom5.png" width="200px">
<img src="https://i.imgur.com/Ab9eXIk.png" width="200px"></center>

<br>

**Computer users:** To start learning locations simply use `./fingerprint -e`.

## 3\. Track yourself

Once you've collected data in a few locations, you can track yourself.

**Android users:** Just press the "Track" button when you're ready to track.

**Computer users:** Type in `./fingerprint` to start tracking yourself.