+++
title = "Contact"
date = "2016-12-25"
sidemenu = "true"
description = "How to contact me"
keywordlist = "contact, home automation, home assistant, openhab, particle, esp8266, internal nagivation, indoor positioning, positioning"
+++

<div id="mc_embed_signup">
<form class="pure-form pure-form-stacked" action="https://formspree.io/hypercubeplatforms@gmail.com" method="POST">
<h2>Feel free to send me a message</h2>
  <fieldset>
    <div class="pure-g">
      <div class="pure-u-1-2">
        <label for="first-name">Name</label>
        <input id="first-name" name="name" class="pure-u-23-24" type="text">
      </div>
      <div class="pure-u-1-2">
        <label for="email">E-Mail</label>
        <input id="email" name="email" class="pure-u-23-24" type="email" required>
      </div>
    <div class="pure-u-1">
      <textarea id="message" name="message" class="pure-input-1" placeholder="Your message" rows=10></textarea>
    </div>
    <button type="submit" class="pure-input-1 pure-button pure-button-primary">Send</button>
  </fieldset>
</form>
</div>


<!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
  #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
  /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
     We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="//hypercubeplatforms.us10.list-manage.com/subscribe/post?u=885d1826479b36238603d935c&amp;id=dfc8e534c4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
  <h2>Subscribe to the FIND mailing list</h2>
<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
<div class="mc-field-group">
  <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
</label>
  <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
  <div id="mce-responses" class="clear">
    <div class="response" id="mce-error-response" style="display:none"></div>
    <div class="response" id="mce-success-response" style="display:none"></div>
  </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_885d1826479b36238603d935c_dfc8e534c4" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->