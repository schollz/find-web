# README

The docs for the splash page (https://www.internalpositioning.com/) are in `/splashsite`.  All other docs are in `/docs`.

To use, make sure you [download the latest version of Hugo](https://github.com/spf13/hugo/releases).

Then, simply do `make` and open your browser to http://localhost:8000.

